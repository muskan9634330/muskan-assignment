package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginPage
{
WebDriver driver;

public LoginPage(WebDriver driver)
{
	this.driver=driver;
	PageFactory.initElements(driver, this);
}
@FindBy(xpath="//input[@id='user_email']")
WebElement emailId;

@FindBy(xpath="//input[@id='user_password']")
WebElement password;


@FindBy(xpath="//div[text()='Log in']//parent::button")
WebElement loginButton;

public void enterUsername()
{
	emailId.sendKeys("tester@company.com");
}

public void enterPassword()
{
	password.sendKeys("Pass@4321$");
}

public void clickLogin()
{
	loginButton.click();
}


}
