package PageObjects;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import abstractComponents.AbstractComponents;

public class HomePage extends AbstractComponents

{
	WebDriver driver;
	Actions action;

	public HomePage(WebDriver driver)
	{
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//div[contains(@id,\"group_title\")]//input[@placeholder='+ Add budget']")
	WebElement addBudget;
	
	@FindBy(xpath="(//div[contains(@class,'last-pulse')])[1]/div/div/div[2] //div[contains(@class,'col-identifier-person')]/div/div/div/div")
	WebElement person;

	@FindBy(xpath="(//div[contains(@class,'last-pulse')])[1]/div/div/div[2] //div[contains(@class,'col-identifier-person')]/div/div/div/div/i")
	WebElement addButtonOfPerson;
	
	@FindBy(xpath="//div[@id='combobox-item-45697786']")
	WebElement newOptionOfPerson;
	
	@FindBy(xpath="(//div[contains(@class,'last-pulse')])[1]/div/div/div[2] //div[contains(@class,'col-identifier-status')]/div/div/div/div[2]")
	WebElement clickStatus;
	
	@FindBy(xpath="//ul[@class=\"status-picker-colors-view\"]/li")
	List<WebElement> getAllStatusOptions;
	
	@FindBy(xpath="//span[text()='Working on it']//parent::div")
	WebElement workingOnIt;
	
	@FindBy(xpath="(//div[contains(@class,'last-pulse')])[1]/div/div/div[2] //div[contains(@class,'col-identifier-date4')]/div/div[1]/div")
	WebElement clickDate;
	
	@FindBy(xpath="//div[@class=\"CalendarMonth CalendarMonth--horizontal\" and @data-visible=\"true\"]/div/strong/div/div[1]")
	WebElement currentMonth;
	
	@FindBy(xpath="//div[@class=\"CalendarMonth CalendarMonth--horizontal\" and @data-visible=\"true\"] //tbody[@class=\"js-CalendarMonth__grid\"]/tr")
	List<WebElement> printAllDates;
	
	
	
	public void addBudget() throws InterruptedException, IOException
	{
		action = new Actions(driver); 
		Thread.sleep(8000);
		action.sendKeys(Keys.PAGE_DOWN).build().perform();
		waitForelementToBeClickable(addBudget);
		addBudget.click();
		addBudget.sendKeys("Assignment");
		getScreenshot("addBudget",driver);
		action.sendKeys(Keys.ENTER).build().perform();
	}
	
	public void addPerson() throws IOException
	{   
		waitForelementToBeClickable(person);
		action.moveToElement(person).build().perform();
		addButtonOfPerson.click();
		newOptionOfPerson.click();
		getScreenshot("addPerson",driver);
	}
	
	public void addStatus() throws InterruptedException
	{  
		waitForelementToBeClickable(clickStatus);
		clickStatus.click();
		Thread.sleep(1000);
		for(WebElement option:getAllStatusOptions){
			if(option.getText().equalsIgnoreCase("Working On It")) {
			option.click();
			break;}}
	}
	
	
	public void addDate() throws InterruptedException
	{
		waitForelementToBeClickable(clickDate);
		clickDate.click();
		System.out.println("Printing current month and dates here");
		System.out.println(currentMonth.getText());
		for(WebElement dates:printAllDates){
			System.out.println(dates.getText());}
	}
	
	

}
