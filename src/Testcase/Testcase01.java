package Testcase;
import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import BaseData.BaseTest;
import PageObjects.HomePage;
import PageObjects.LoginPage;

public class Testcase01 extends BaseTest

{
@Test
public void TC01() throws IOException, InterruptedException
{
	WebDriver driver=BaseTest();
	HomePage home=new HomePage(driver);
	LoginPage login=new LoginPage(driver); 
	login.enterUsername();
	login.enterPassword();
	login.clickLogin();
	home.addBudget();
	home.addPerson();
	home.addStatus();
	home.addDate();	
	driver.quit();
}
}
